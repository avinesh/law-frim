<?php include 'header.php' ?>
    <!--
     Banner slider starts here
    -->
<div class="banner_slider_section ">
    <div id="spl_law_bannerSlider" class="spl_law_bannerSlider owl-carousel features-slider">
        <!-- slider item start -->
        <div class="banner-item" style="background-image: url(assets/images/slider-1.png);">
            <div class="bannerOuter">
                <div class="bannerInner">
                    <div class="thin_layer" style="background: #000;opacity: 0.2"></div>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="slider-content ">
                                    <h2 class="banner-title">
                                        Committed to Helping Our Clients Succeed
                                    </h2>
                                    <p>
                                        Benefit of the socie where we operate. A success website obusly needs great
                                        design to be one top10 IT companies in The world
                                    </p>
                                    <div class="btn-area">
                                        <a href="#" class="btn btn-str-up">
                                            Our Servies
                                            <i class="icon icon-arrow-right"></i>
                                        </a>
                                    </div>
                                </div> <!-- slider content end-->
                            </div> <!-- col end-->
                        </div> <!-- row end-->
                    </div>
                    <!-- container end -->
                </div>
                <!-- hero table cell end -->
            </div>
            <!-- hero table end -->
        </div>
        <div class="banner-item" style="background-image: url(assets/images/slider-3.png);">
            <div class="bannerOuter">
                <div class="bannerInner">
                    <div class="thin_layer" style="background: #000;opacity: 0.2"></div>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="slider-content ">
                                    <h2 class="banner-title">
                                        Insurance Law Expertise…It’s Our Policy
                                    </h2>
                                    <p>
                                        Benefit of the socie where we operate. A success website obusly needs great
                                        design to be one top10 IT companies in The world
                                    </p>
                                    <div class="btn-area">
                                        <a href="#" class="btn btn-str-up">
                                            Our Servies
                                            <i class="icon icon-arrow-right"></i>
                                        </a>
                                    </div>
                                </div> <!-- slider content end-->
                            </div> <!-- col end-->
                        </div> <!-- row end-->
                    </div>
                    <!-- container end -->
                </div>
                <!-- hero table cell end -->
            </div>
            <!-- hero table end -->
        </div>
        <div class="banner-item" style="background-image: url(assets/images/slider-3.png);">
            <div class="bannerOuter">
                <div class="bannerInner">
                    <div class="thin_layer" style="background: #000;opacity: 0.2"></div>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="slider-content ">
                                    <h2 class="banner-title">
                                        Insurance Law Expertise…It’s Our Policy
                                    </h2>
                                    <p>
                                        Benefit of the socie where we operate. A success website obusly needs great
                                        design to be one top10 IT companies in The world
                                    </p>
                                    <div class="btn-area">
                                        <a href="#" class="btn btn-str-up">
                                            Our Servies
                                            <i class="icon icon-arrow-right"></i>
                                        </a>
                                    </div>
                                </div> <!-- slider content end-->
                            </div> <!-- col end-->
                        </div> <!-- row end-->
                    </div>
                    <!-- container end -->
                </div>
                <!-- hero table cell end -->
            </div>
            <!-- hero table end -->
        </div>
    </div>
</div>
    <!-- Slider section end -->

    <!--
     Feature  Services
    -->

    <section class="spl_law_feature_services">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="feature_service">
                        <i class="fas fa-gavel"></i>
                        <h4><a href="#">2020 WINNING AWARD</a></h4>
                        <p>Ante in eu porta mi, dolor et amete iaculis, sapien proin auctor enint phasn leo
                            ailoremfeugiat and over the world to be grow</p>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="feature_service">
                        <i class="far fa-handshake"></i>
                        <h4><a href="#">Free Counceling</a></h4>
                        <p>Ante in eu porta mi, dolor et amete iaculis, sapien proin auctor enint phasn leo
                            ailoremfeugiat and over the world to be grow</p>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="feature_service">
                        <i class="fas fa-user-shield"></i>
                        <h4><a href="#">Private</a></h4>
                        <p>Ante in eu porta mi, dolor et amete iaculis, sapien proin auctor enint phasn leo
                            ailoremfeugiat and over the world to be grow</p>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="spl_law_about_front">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-md-6 col-sm-12 wow fadeInLeft" data-wow-duration="0.9s">
                    <div class="img-background-wrapper">
                        <div class="img_background" style="background-image: url(assets/images/aboutus.png)">
                        </div>
                    </div>
                </div>

                <div class="col-lg-7 col-md-6  col-sm-12">
                    <h2>LAW ADVICE FOR OUR CLIENT</h2>
                    <h3>We will always find a way out for the solution of the problem.</h3>
                    <p>
                        Nulla ut fringilla dui. Nullam sit amet pulvinar velit, at rutrum lacus. Aliquam uidem rerum
                        facilis
                        est et expedita distinctio. Nam libero tempore, cum soluta Nulla ut fringilla dui.
                        Nulla ut fringilla dui. Nullam sit amet pulvinar velit, at rutrum lacus. Aliquam uidem rerum
                        facilis est et expedita distinctio. Nam libero tempore, cum soluta Nulla ut fringilla dui.
                    </p>
                    <div class="address-info">
                        <ul>
                            <li>sit amet pulvinar</li>
                            <li>only service you need</li>
                            <li>Dont worry be happy</li>
                            <li>All We Do is Work</li>
                        </ul>
                    </div>
                    <div class="contact">Call to ask any question <a href="#">680-325-1523</a></div>
                    <div class="signature">
                        <div class="signature-img"><img src="assets/images/signature.png" alt=""></div>
                        <h4>Natalia Duke</h4>
                        <div class="designation">(Chairman and founder)</div>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <section class="spl_law_practise_area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="section-title-wrapper">
                        <h2 class="section-title">Our Practise Area</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="practise_area wow fadeInUp">
                        <div class="icon-area">
                            <i class="icofont-pistol"></i>
                        </div>
                        <h3><a href="#">Gun Law</a></h3>
                        <p>Sustainable development is the pathway to the future</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 ">
                    <div class="practise_area wow fadeInUp" data-wow-delay="0.3s">
                        <div class="icon-area">
                            <i class="fas fa-child"></i>
                        </div>
                        <h3><a href="#">Child Support</a></h3>
                        <p>Sustainable development is the pathway to the future</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 ">
                    <div class="practise_area  wow fadeInUp" data-wow-delay="0.6s">
                        <div class="icon-area">
                            <i class="fas fa-venus-mars"></i></div>
                        <h3><a href="#">Divorce</a></h3>
                        <p>Sustainable development is the pathway to the future</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 ">
                    <div class="practise_area  wow fadeInUp" data-wow-delay="0s">
                        <div class="icon-area">
                            <i class="fas fa-user-secret"></i>
                        </div>
                        <h3><a href="#">Cyber Crime Law</a></h3>
                        <p>Sustainable development is the pathway to the future</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 ">
                    <div class="practise_area  wow fadeInUp" data-wow-delay="0.3s">
                        <div class="icon-area">
                            <i class="icofont-handcuff"></i>
                        </div>
                        <h3><a href="#">Serious Crime</a></h3>
                        <p>Sustainable development is the pathway to the future</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 ">
                    <div class="practise_area wow fadeInUp" data-wow-delay="0.6s">
                        <div class="icon-area">
                            <i class="icofont-thief"></i>
                        </div>
                        <h3><a href="#">Robbery</a></h3>
                        <p>Sustainable development is the pathway to the future</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="spl_law_case_study">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title-wrapper">
                        <h2 class="section-title">Case Studies</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="portfolio">

                        <ul class="text-center">
                            <li class="active">
                                <button data-filter="*">All</button>
                            </li>
                            <li>
                                <button data-filter=".wordpress">wordpress</button>
                            </li>
                            <li>
                                <button data-filter=".vb">vb</button>
                            </li>
                            <li>
                                <button data-filter=".joomla">joomla</button>
                            </li>
                        </ul>

                        <div class="portfolio-grid">
                            <div class="portfolio-wrap">
                                <div class="col-lg-4 col-md-4 col-sm-12 image-list wordpress">
                                    <div class="image-list"><img src="assets/images/case_1.jpg"
                                                                 alt="#"/>
                                        <div class="portfolio-overlay">
                                            <div class="portfolio-overlay-inner">
                                                <div class="portfolio-overlay-content">
                                                    <div class="portfolio-link">
                                                        <a title="Details" href="#"><i
                                                                    class="fa fa-link"></i></a>
                                                        <a href="#"><i
                                                                    class="fa fa-search-plus"></i></a>
                                                    </div><!--End portfolio-link -->
                                                    <div class="portfolio-caption">
                                                        <h3><a href="javascript:void(0)">Project Title5</a></h3>
                                                    </div><!--End portfolio-caption -->
                                                </div><!--End portfolio-overlay-content -->
                                            </div><!--End portfolio-overlay-inner -->
                                        </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 image-list vb joomla">
                                    <div class="image-list"><img src="assets/images/case_2.jpg"
                                                                 alt="#"/>
                                        <div class="portfolio-overlay">
                                            <div class="portfolio-overlay-inner">
                                                <div class="portfolio-overlay-content">
                                                    <div class="portfolio-link">
                                                        <a title="Details" href="#"><i
                                                                    class="fa fa-link"></i></a>
                                                        <a href="#"><i
                                                                    class="fa fa-search-plus"></i></a>
                                                    </div><!--End portfolio-link -->
                                                    <div class="portfolio-caption">
                                                        <h3><a href="javascript:void(0)">Project Title5</a></h3>
                                                    </div><!--End portfolio-caption -->
                                                </div><!--End portfolio-overlay-content -->
                                            </div><!--End portfolio-overlay-inner -->
                                        </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 image-list wordpress">
                                    <div class="image-list"><img src="assets/images/case_3.jpg"
                                                                 alt="#"/>
                                        <div class="portfolio-overlay">
                                            <div class="portfolio-overlay-inner">
                                                <div class="portfolio-overlay-content">
                                                    <div class="portfolio-link">
                                                        <a title="Details" href="#"><i
                                                                    class="fa fa-link"></i></a>
                                                        <a href="#"><i
                                                                    class="fa fa-search-plus"></i></a>
                                                    </div><!--End portfolio-link -->
                                                    <div class="portfolio-caption">
                                                        <h3><a href="javascript:void(0)">Project Title5</a></h3>
                                                    </div><!--End portfolio-caption -->
                                                </div><!--End portfolio-overlay-content -->
                                            </div><!--End portfolio-overlay-inner -->
                                        </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 image-list vb">
                                    <div class="image-list"><img src="assets/images/case_4.jpg"
                                                                 alt="#"/>
                                        <div class="portfolio-overlay">
                                            <div class="portfolio-overlay-inner">
                                                <div class="portfolio-overlay-content">
                                                    <div class="portfolio-link">
                                                        <a title="Details" href="#"><i
                                                                    class="fa fa-link"></i></a>
                                                        <a href="#"><i
                                                                    class="fa fa-search-plus"></i></a>
                                                    </div><!--End portfolio-link -->
                                                    <div class="portfolio-caption">
                                                        <h3><a href="javascript:void(0)">Project Title5</a></h3>
                                                    </div><!--End portfolio-caption -->
                                                </div><!--End portfolio-overlay-content -->
                                            </div><!--End portfolio-overlay-inner -->
                                        </div>
                                        </a>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-12 image-list wordpress joomla">
                                    <div class="image-list"><img src="assets/images/case_5.jpg"
                                                                 alt="#"/>
                                        <div class="portfolio-overlay">
                                            <div class="portfolio-overlay-inner">
                                                <div class="portfolio-overlay-content">
                                                    <div class="portfolio-link">
                                                        <a title="Details" href="#"><i
                                                                    class="fa fa-link"></i></a>
                                                        <a href="#"><i
                                                                    class="fa fa-search-plus"></i></a>
                                                    </div><!--End portfolio-link -->
                                                    <div class="portfolio-caption">
                                                        <h3><a href="javascript:void(0)">Project Title5</a></h3>
                                                    </div><!--End portfolio-caption -->
                                                </div><!--End portfolio-overlay-content -->
                                            </div><!--End portfolio-overlay-inner -->
                                        </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 image-list vb joomla">
                                    <div class="image-list"><img src="assets/images/case_6.jpg"
                                                                 alt="#"/>
                                        <div class="portfolio-overlay">
                                            <div class="portfolio-overlay-inner">
                                                <div class="portfolio-overlay-content">
                                                    <div class="portfolio-link">
                                                        <a title="Details" href="#"><i
                                                                    class="fa fa-link"></i></a>
                                                        <a href="#"><i
                                                                    class="fa fa-search-plus"></i></a>
                                                    </div><!--End portfolio-link -->
                                                    <div class="portfolio-caption">
                                                        <h3><a href="javascript:void(0)">Project Title5</a></h3>
                                                    </div><!--End portfolio-caption -->
                                                </div><!--End portfolio-overlay-content -->
                                            </div><!--End portfolio-overlay-inner -->
                                        </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </section>


    <section class="spl_law_contact_detail"
             style="background: url(assets/images/contactus_bg.png) no-repeat;background-size: cover">
        <div class="thin_layer" style="opacity: 0.8;background: #000"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <h2>Are You Interest To Contact With Us?</h2>
                    <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of
                        classical Latin literature from 45 BC.</p>
                    <div class="add-contact">
                        <span class="call-us wow fadeInUp" data-wow-delay="0.1s"><i class="fas fa-map-marker-alt"></i><div
                                    class="contact_info_detail">
                                <h4>Our Location</h4>
                                <span><a href="#">New Road, Touterie Victoria 8520 Australia</a></span>
                            </div></span>
                        <span class="call-us wow fadeInUp" data-wow-delay="0.3s"><i class="fas fa-phone-volume"></i><div
                                    class="contact_info_detail">
                                <h4>Our Location</h4>
                                <span><a href="#">+987652541</a></span>
                            </div></span>
                        <span class="call-us wow fadeInUp" data-wow-delay="0.5s"><i class="fas fa-envelope"></i><div
                                    class="contact_info_detail">
                                <h4>Email</h4>
                                <span><a href="#">info@hehe.com</a></span>
                            </div></span>

                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 ">
                    <div class="contact_form">
                    <div role="form" class="wpcf7" id="wpcf7-f59-o1" lang="en-US" dir="ltr">
                        <div class="screen-reader-response"></div>
                        <h3 class="text-center">REQUEST A QUOTE</h3>
                        <form action="" method="post" class="wpcf7-form" novalidate="novalidate">
                            <div style="display: none;">
                                <input type="hidden" name="_wpcf7" value="59">
                                <input type="hidden" name="_wpcf7_version" value="5.1.1">
                                <input type="hidden" name="_wpcf7_locale" value="en_US">
                                <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f59-o1">
                                <input type="hidden" name="_wpcf7_container_post" value="0">
                                <input type="hidden" name="g-recaptcha-response" value="">
                            </div>
                            <p><label>
                                <span class="wpcf7-form-control-wrap your-name">
                                    <input type="text" class="wpcf7-form-control wpcf7-text" placeholder="Your Name">
                                </span>
                                </label>
                            </p>
                            <p><label>
                                <span class="wpcf7-form-control-wrap your-email">
                                    <input type="email" name="your-email" class="wpcf7-form-control wpcf7-text"
                                           placeholder="Your Email (required)">
                                </span>
                                </label>
                            </p>
                            <p><label>
                                <span class="wpcf7-form-control-wrap your-subject">
                                    <input type="text" name="your-subject" value=""
                                           class="wpcf7-form-control wpcf7-text" placeholder="Subject">
                                </span>
                                </label>
                            </p>
                            <p><label>
                                <span class="wpcf7-form-control-wrap your-message">
                                    <textarea name="your-message" class="wpcf7-form-control wpcf7-textarea"
                                              placeholder="Message"></textarea>
                                </span>
                                </label></p>
                            <p><input type="submit" value="Apponment" class="wpcf7-form-control wpcf7-submit"><span
                                        class="ajax-loader"></span></p>

                        </form>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="spl_law_team">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <h2 class="big-title wow fadeInUp">Our Team </h2>
                    <p class="wow fadeInUp" data-wow-delay="0.3s">The area of law you fancy going into becomes hugely
                        important when it comes to applying for
                        chambers and law firms.</p>
                </div>
                <div class="col-lg-8 col-md-6 col-sm-12">

                    <div class="owl-carousel  team_slider wow fadeInUp" data-wow-delay="0.3s">
                        <div class="item">
                            <div class="team-box">
                                <div class="team-image">
                                    <div class="img-background-wrapper">
                                        <div class="img_background" style="background-image: url(assets/images/team-1.png)">
                                        </div>
                                    </div>
                                    <ul class="social-links">
                                        <li><a href="#"><i class="fab fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                                    </ul>
                                </div>

                                <div class="team-content">
                                    <h3 class="team-title"><a href="#">Jennifer Aniston</a></h3>
                                    <span>Women Rights Lawyer</span>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="team-box">
                                <div class="team-image">
                                    <div class="img-background-wrapper">
                                        <div class="img_background" style="background-image: url(assets/images/team-2.png)">
                                        </div>
                                    </div>
                                    <ul class="social-links">
                                        <li><a href="#"><i class="fab fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                                    </ul>
                                </div>

                                <div class="team-content">
                                    <h3 class="team-title"><a href="#">Jennifer Aniston</a></h3>
                                    <span>Women Rights Lawyer</span>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="team-box">
                                <div class="team-image">
                                    <div class="img-background-wrapper">
                                        <div class="img_background" style="background-image: url(assets/images/team-3.png)">
                                        </div>
                                    </div>
                                    <ul class="social-links">
                                        <li><a href="#"><i class="fab fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                                    </ul>
                                </div>

                                <div class="team-content">
                                    <h3 class="team-title"><a href="#">Jennifer Aniston</a></h3>
                                    <span>Women Rights Lawyer</span>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="team-box">
                                <div class="team-image">
                                    <div class="img-background-wrapper">
                                        <div class="img_background" style="background-image: url(assets/images/team-4.png)">
                                        </div>
                                    </div>
                                    <ul class="social-links">
                                        <li><a href="#"><i class="fab fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                                    </ul>
                                </div>

                                <div class="team-content">
                                    <h3 class="team-title"><a href="#">Jennifer Aniston</a></h3>
                                    <span>Women Rights Lawyer</span>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <section class="spl_law_video">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="section-title-wrapper">
                        <h2 class="section-title">Frequently Asked Question</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="video wow fadeInUp">
                        <div class="img_background" style="background-image: url(assets/images/video.png)">
                        </div>
                        <div class="thin_layer" style="background: #000;opacity: 0.6"></div>
                        <div class="button-video">
                            <a href="http://www.youtube.com/watch?v=0O2aH4XLbto" target="_blank"
                               class="popup-youtube  box-shadow-ripples"><i class="fas fa-play "></i></a>
                        </div>
                    </div>

                </div>
                <div class="col-lg-6 col-md-6 col-sm-12">


                    <div class="accordion-container">
                        <div class="set wow fadeInUp">
                            <a href="javascript:void(0)" class="active">
                                Vestibulum
                            </a>
                            <div class="content" style="display: block">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                    incididunt ut labore et dolore magna.Lorem ipsum dolor sit amet, consectetur
                                    adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                            </div>
                        </div>
                        <div class="set wow fadeInUp" data-wow-delay="0.3s">
                            <a href="javascript:void(0)">
                                Phasellus
                            </a>
                            <div class="content">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                    incididunt ut labore et dolore magna.Lorem ipsum dolor sit amet, consectetur
                                    adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                            </div>
                        </div>
                        <div class="set wow fadeInUp" data-wow-delay="0.5s">
                            <a href="javascript:void(0)">
                                Praesent
                            </a>
                            <div class="content">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                    incididunt ut labore et dolore magna.Lorem ipsum dolor sit amet, consectetur
                                    adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                            </div>
                        </div>
                        <div class="set wow fadeInUp" data-wow-delay="0.7s">
                            <a href="javascript:void(0)">
                                Curabitur
                            </a>
                            <div class="content">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                    incididunt ut labore et dolore magna.Lorem ipsum dolor sit amet, consectetur
                                    adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="spl_law_testimonial" style="background: url(assets/images/testimonial-bg-2.jpg) no-repeat bottom;background-size:cover;">
        <div class="thin_layer" style="background: #000;opacity:0.8"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-6 col-sm-12">
                    <div class="owl-carousel owl-theme testimonial_slider">
                        <div class="item">
                            <div class="testimonial_inner">
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod
                                    ncididunt ametfh consectetur dolore magna aliqua.</p>
                                <div class="testimonial_author">
                                    <div class="author_image">
                                        <img src="assets/images/testimonial-1.jpg" alt="author image">
                                    </div>
                                    <h5>Nancy Wise</h5>
                                    <p>Web Developer</p>
                                </div>
                                <i class="fas fa-quote-left"></i>
                            </div>
                        </div>
                        <div class="item">
                            <div class="testimonial_inner">
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod
                                    ncididunt ametfh consectetur dolore magna aliqua.</p>
                                <div class="testimonial_author">
                                    <div class="author_image">
                                        <img src="assets/images/testimonial-2.jpg" alt="author image">
                                    </div>
                                    <h5>Nancy Wise</h5>
                                    <p>Web Developer</p>
                                </div>
                                <i class="fas fa-quote-left"></i>
                            </div>
                        </div>
                        <div class="item">
                            <div class="testimonial_inner">
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod
                                    ncididunt ametfh consectetur dolore magna aliqua.</p>
                                <div class="testimonial_author">
                                    <div class="author_image">
                                        <img src="assets/images/testimonial-3.jpg" alt="author image">
                                    </div>
                                    <h5>Nancy Wise</h5>
                                    <p>Web Developer</p>
                                </div>
                                <i class="fas fa-quote-left"></i>
                            </div>
                        </div>
                        <div class="item">
                            <div class="testimonial_inner">
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod
                                    ncididunt ametfh consectetur dolore magna aliqua.</p>
                                <div class="testimonial_author">
                                    <div class="author_image">
                                        <img src="assets/images/testimonial-4.jpg" alt="author image">
                                    </div>
                                    <h5>Nancy Wise</h5>
                                    <p>Web Developer</p>
                                </div>
                                <i class="fas fa-quote-left"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <h2 class="big-title white">Our Testimonials </h2>
                    <p>The area of law you fancy going into becomes hugely important when it comes to applying for
                        chambers and law firms.</p>
                </div>
            </div>
        </div>
    </section>
    <section class="blog">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="section-title-wrapper">
                        <h2 class="section-title">Our Blog</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <article class="blog_list_post">
                        <div class="img-background-wrapper">
                            <div class="img_background shine"
                                 style="background-image: url(assets/images/about_bg.jpg)">
                            </div>
                        </div>
                        <div class="box-conetnt">
                            <div class="post-info-date">
                                <span class="post-info-date-day">18</span>
                                <span class="post-info-date-month">Jun</span>
                            </div>
                            <div class="video-content">
                                <h3><a href="#">Training in The Law in order to become </a> </h3>
                                <div class="post_meta"><span><i class="far fa-folder-open"></i></span> in <span>Travel, law , contract</span>
                                </div>
                                <p>Maybe it won’t get that far, but those who care about these international law
                                    disputes think China and the U.S. are on a collision course because both sides hew </p>
                            </div>
                        </div>
                    </article>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <article class=" blog_list_post">
                        <div class="img-background-wrapper">
                            <div class="img_background shine"
                                 style="background-image: url(assets/images/about_bg.jpg)">
                            </div>
                        </div>
                        <div class="box-conetnt">
                            <div class="post-info-date">
                                <span class="post-info-date-day">18</span>
                                <span class="post-info-date-month">Jun</span>
                            </div>
                            <div class="video-content">
                                <h3><a href="#">Training in The Law in order to become  </a> </h3>
                                <div class="post_meta"><span><i class="far fa-folder-open"></i></span> in <span>Travel, law , contract</span>
                                </div>
                                <p>Maybe it won’t get that far, but those who care about these international law
                                    disputes think China and the U.S. are on a collision course because both sides hew </p>
                            </div>
                        </div>
                    </article>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <article class=" blog_list_post">
                        <div class="img-background-wrapper">
                            <div class="img_background shine"
                                 style="background-image: url(assets/images/about_bg.jpg)">
                            </div>
                        </div>
                        <div class="box-conetnt">
                            <div class="post-info-date">
                                <span class="post-info-date-day">18</span>
                                <span class="post-info-date-month">Jun</span>
                            </div>
                            <div class="video-content">
                                <h3><a href="#">Training in The Law in order to become </a> </h3>
                                <div class="post_meta"><span><i class="far fa-folder-open"></i></span> in <span>Travel, law , contract</span>
                                </div>
                                <p>Maybe it won’t get that far, but those who care about these international law
                                    disputes think China and the U.S. are on a collision course because both sides hew </p>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </section>
<?php include 'footer.php' ?>