</div><!-- #content -->


<footer id="colophon" class="site-footer">
    <div class="upper-footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <div id="custom_html-3" class="widget_text widget widget_custom_html"><h2 class="widget-title">
                            LAWYER
                            FRIM</h2>
                        <div class="textwidget custom-html-widget">Finance Group delivers environment through which
                            high-quality firms from diverse backgrounds can work together toward their own common goals.
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">

                    <div class="widget custom-recent-post">
                        <h2 class="widget-title">Recent Posts</h2>
                        <ul>
                            <li>
                                <div class="thumb">
                                    <a href="#">
                                        <img src="assets/images/recent-post-1.jpg" alt="Thumb">
                                    </a>
                                </div>
                                <div class="info">
                                    <a href="#">Common so wicket appear to sudden</a>
                                    <div class="meta-title">
                                        <span class="post-date">March 6, 2019</span> - By <a
                                                href="#" title="Posts by admin"
                                                rel="author">admin</a></div>
                                </div>
                            </li>
                            <li>
                                <div class="thumb">
                                    <a href="#">
                                        <img src="assets/images/recent-post-2.jpg" alt="Thumb">
                                    </a>
                                </div>
                                <div class="info">
                                    <a href="#">Announcing me stimulated
                                        continuing</a>
                                    <div class="meta-title">
                                        <span class="post-date">March 6, 2019</span> - By <a
                                                href="https://sttheme.com/demosd/busprowp/?author=1"
                                                title="Posts by admin"
                                                rel="author">admin</a></div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <div id="recent-comments-3" class="widget widget_recent_comments"><h2 class="widget-title">RECENT
                            COMMENTS</h2>
                        <ul id="recentcomments">
                            <li class="recentcomments"><span class="comment-author-link">admin</span> on <a
                                        href="#">Magni
                                    dolores</a></li>
                            <li class="recentcomments"><span class="comment-author-link">admin</span> on <a
                                        href="#">Magni
                                    dolores</a></li>
                            <li class="recentcomments"><span class="comment-author-link">admin</span> on <a
                                        href="#">Magni
                                    dolores</a></li>
                            <li class="recentcomments"><span class="comment-author-link">admin</span> on <a
                                        href="#">Magni
                                    dolores</a></li>
                            <li class="recentcomments"><span class="comment-author-link"><a href=#"
                                                                                            rel="external nofollow"
                                                                                            class="url">A WordPress Commenter</a></span>
                                on <a href="#">Hello
                                    world!</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <div id="categories-4" class="widget widget_categories"><h2 class="widget-title">Category</h2>
                        <ul>
                            <li class="cat-item cat-item-3"><a
                                        href="#">Construction</a> (3)
                            </li>
                            <li class="cat-item cat-item-7"><a
                                        href="#">Photography</a> (3)
                            </li>
                            <li class="cat-item cat-item-1"><a
                                        href="#">Uncategorized</a>
                                (3)
                            </li>
                            <li class="cat-item cat-item-2"><a
                                        href="#">Web</a> (4)
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="lower-footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <p class="copyright">Copyright © 2019 Sparkle Themes. All rights reserved</p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <ul class="social-media">
                        <li><a href="#"><i class="fab fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <a href="#" class="back-to-top"><i class="icofont-thin-up"></i></a>
    </div>

</footer>
</div><!-- #page -->

<script src="assets/js/jquery.min.js"></script>
<script src="assets/library/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/library/owlcarousel/js/owl.carousel.min.js"></script>
<script src="assets/js/waypoints.min.js"></script>
<script src="assets/library/counter/jquery.counterup.min.js"></script>
<script src="assets/js/jquery.isotope.js"></script>
<script src="assets/js/wow.js"></script>
<script src="assets/library/magnific-popup/magnific-popup.min.js"></script>
<script src="assets/js/main.js"></script>
</body>

</html>