

$(document).ready(function () {

    $('.carousel-control-prev-icon').append('<i class="icofont-thin-left"></i>');
    $('.carousel-control-next-icon').append('<i class="icofont-thin-right"></i>');


    /********************
     *  banner
     * *****************/
    $('.spl_law_bannerSlider').owlCarousel({
        loop: true,
        dots: true,
        autoplay: false,
        smartSpeed:2000,
        nav: true,
        navText: ["<i class='fas fa-angle-left'></i>", "<i class='fas fa-angle-right'></i>"],
        items: 1
    });
    /********************
     * Counter
     * *****************/
    $('.counter').counterUp();

    /********************
     *  portfolio
     * *****************/
    var $portfolio = $(".portfolio-grid .portfolio-wrap");
    $portfolio.isotope({
        filter: '*',
        animationOptions: {
            duration: 750,
            easing: 'linear',
            queue: false
        }});
    $('.spl_law_case_study ul li').click(function(e){
        $('.spl_law_case_study ul li.active').removeClass('active');
        $(this).addClass('active');
        var filterValue = $(this).children().attr("data-filter");
        $portfolio.isotope({
            filter: filterValue,
            animationOptions: {
                duration: 750,
                easing: 'linear',
                queue: false
            }
        });

    });


    /********************
     *  Accordion
     * *****************/

    $(".set > a").on("click", function() {
        if ($(this).hasClass("active")) {
            $(this).removeClass("active");
            $(this)
                .siblings(".content")
                .slideUp(200);

        } else {

            $(".set > a").removeClass("active");
            $(this).addClass("active");
            $(".content").slideUp(200);
            $(this)
                .siblings(".content")
                .slideDown(200);
        }
    });
    /********************
     *  team
     * *****************/
    $('.team_slider').owlCarousel({
        loop: true,
        margin: 10,
        dots: false,
        autoplay: false,
        smartSpeed:2000,
        nav: true,
        navText: ["<i class='icofont-thin-left'></i>", "<i class='icofont-thin-right'></i>"],
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 2
            }
        }
    });

    $('.team-carousel').owlCarousel({
        loop: true,
        margin: 10,
        dots: false,
        autoplay: false,
        smartSpeed:2000,
        nav: true,
        navText: ["<i class='icofont-thin-left'></i>", "<i class='icofont-thin-right'></i>"],
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 4
            },
            1200: {
                items: 5
            }
        }
    });


    $('.team-carousel-layout-three').owlCarousel({
        loop: true,
        margin: 10,
        dots: false,
        autoplay: false,
        smartSpeed:2000,
        nav: true,
        navText: ["<i class='icofont-thin-left'></i>", "<i class='icofont-thin-right'></i>"],
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 2
            }
        }
    });



    /********************
     *  testimonial
     * *****************/
    $('.testimonial_slider').owlCarousel({
        loop: true,
        margin: 10,
        dots: false,
        autoplay: false,
        smartSpeed:2000,
        nav: true,
        navText: ["<i class='icofont-thin-left'></i>", "<i class='icofont-thin-right'></i>"],
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    });

    /********************
     * Video popup
     * *****************/


    $('.popup-youtube').magnificPopup({
        disableOn: 700,
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,

        fixedContentPos: false
    });

    /********************
     *  init wow js
     * *****************/
    var wow = new WOW(
        {
            boxClass: 'wow',      // animated element css class (default is wow)
            animateClass: 'animated', // animation css class (default is animated)
            offset: 0,          // distance to the element when triggering the animation (default is 0)
            mobile: true,       // trigger animations on mobile devices (default is true)
            live: true,       // act on asynchronously loaded content (default is true)
            callback: function (box) {
                // the callback is fired every time an animation is started
                // the argument that is passed in is the DOM node being animated
            },
            scrollContainer: null,    // optional scroll container selector, otherwise use window,
            resetAnimation: true,     // reset animation on end (default is true)
        }
    );
    wow.init();

    /********************
     *  back to top
     * *****************/
    $('.back-to-top').click(function() {      // When arrow is clicked
        $('body,html').animate({
            scrollTop : 0                       // Scroll to top of body
        }, 3000);
    });

    /********************
     * responsive menu
     * *****************/

    $('.main-menu-toggle').click(function () {
        $(this).parent('.main-navigation').children('.main-menu-container-collapse').first().slideToggle('1000');
    });
    /* **
   * Sub Menu
   **/
    $('nav .menu-item-has-children').append('<span class="sub-toggle"> <i class="fa fa-plus"></i> </span>');
    $('nav .page_item_has_children').append('<span class="sub-toggle-children"> <i class="fa fa-plus"></i> </span>');

    $('nav .sub-toggle').click(function () {
        $(this).parent('.menu-item-has-children').children('ul.sub-menu').slideToggle('1000');
        $(this).children('.fa-plus').first().toggleClass('fa-minus');
    });

    $('.navbar .sub-toggle-children').click(function () {
        $(this).parent('.page_item_has_children').children('ul.sub-menu').slideToggle('1000');
        $(this).children('.fa-plus').first().toggleClass('fa-minus');
    });


    /*------------------------------------
        Search Menu
      --------------------------------------*/
    $('.column-button-search a').click(function () {

        $('.search-pop-up').toggleClass('active');
    });


});