<?php include 'header-pro-1.php' ?>
    <!--
     Banner slider starts here
    -->
    <div class="banner_slider_section form layout_two">
        <div class="consultant_form">
            <div role="form" class="wpcf7" id="wpcf7-f59-o1" lang="en-US" dir="ltr">
                <div class="screen-reader-response"></div>
                <h3 class="text-center">Free Consultation</h3>
                <form action="" method="post" class="wpcf7-form" novalidate="novalidate">
                    <div style="display: none;">
                        <input type="hidden" name="_wpcf7" value="59">
                        <input type="hidden" name="_wpcf7_version" value="5.1.1">
                        <input type="hidden" name="_wpcf7_locale" value="en_US">
                        <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f59-o1">
                        <input type="hidden" name="_wpcf7_container_post" value="0">
                        <input type="hidden" name="g-recaptcha-response" value="">
                    </div>
                    <p><label>
                                <span class="wpcf7-form-control-wrap your-name">
                                    <input type="text" class="wpcf7-form-control wpcf7-text" placeholder="Your Name">
                                </span>
                        </label>
                    </p>
                    <p><label>
                                <span class="wpcf7-form-control-wrap your-email">
                                    <input type="email" name="your-email" class="wpcf7-form-control wpcf7-text" placeholder="Your Email (required)">
                                </span>
                        </label>
                    </p>
                    <p><label>
                                <span class="wpcf7-form-control-wrap your-subject">
                                    <input type="text" name="your-subject" value="" class="wpcf7-form-control wpcf7-text" placeholder="Subject">
                                </span>
                        </label>
                    </p>
                    <p><label>
                                <span class="wpcf7-form-control-wrap your-message">
                                    <textarea name="your-message" class="wpcf7-form-control wpcf7-textarea" placeholder="Message"></textarea>
                                </span>
                        </label></p>
                    <p><input type="submit" value="Apponment" class="wpcf7-form-control wpcf7-submit"><span class="ajax-loader"></span></p>

                </form>
            </div>
        </div>
    <div id="spl_law_bannerSlider" class="spl_law_bannerSlider owl-carousel features-slider">
        <!-- slider item start -->
        <div class="banner-item" style="background-image: url(assets/images/slider-1.png);">
            <div class="bannerOuter">
                <div class="bannerInner">
                    <div class="thin_layer" style="background: #000;opacity: 0.2"></div>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="slider-content center">
                                    <h2 class="banner-title">
                                        Committed to Helping Our Clients Succeed
                                    </h2>
                                    <p>
                                        Benefit of the socie where we operate. A success website obusly needs great
                                        design to be one top10 IT companies in The world
                                    </p>
                                    <div class="btn_type_two">
                                        <a href="#">
                                            <span class="btn_text">Contact Us</span>
                                            <span class="btn_icon"><i class="icofont-rounded-right"></i></span>
                                        </a>
                                    </div>
                                </div> <!-- slider content end-->
                            </div> <!-- col end-->
                        </div> <!-- row end-->
                    </div>
                    <!-- container end -->
                </div>
                <!-- hero table cell end -->
            </div>
            <!-- hero table end -->
        </div>
        <div class="banner-item" style="background-image: url(assets/images/slider-3.png);">
            <div class="bannerOuter">
                <div class="bannerInner">
                    <div class="thin_layer" style="background: #000;opacity: 0.2"></div>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="slider-content center">
                                    <h2 class="banner-title">
                                        Insurance Law Expertise…It’s Our Policy
                                    </h2>
                                    <p>
                                        Benefit of the socie where we operate. A success website obusly needs great
                                        design to be one top10 IT companies in The world
                                    </p>
                                    <div class="btn_type_two">
                                        <a href="#">
                                            <span class="btn_text">Contact Us</span>
                                            <span class="btn_icon"><i class="icofont-rounded-right"></i></span>
                                        </a>
                                    </div>
                                </div> <!-- slider content end-->
                            </div> <!-- col end-->
                        </div> <!-- row end-->
                    </div>
                    <!-- container end -->
                </div>
                <!-- hero table cell end -->
            </div>
            <!-- hero table end -->
        </div>
        <div class="banner-item" style="background-image: url(assets/images/slider-3.png);">
            <div class="bannerOuter">
                <div class="bannerInner">
                    <div class="thin_layer" style="background: #000;opacity: 0.2"></div>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="slider-content center">
                                    <h2 class="banner-title">
                                        Insurance Law Expertise…It’s Our Policy
                                    </h2>
                                    <p>
                                        Benefit of the socie where we operate. A success website obusly needs great
                                        design to be one top10 IT companies in The world
                                    </p>
                                    <div class="btn_type_two">
                                        <a href="#">
                                            <span class="btn_text">Contact Us</span>
                                            <span class="btn_icon"><i class="icofont-rounded-right"></i></span>
                                        </a>
                                    </div>
                                </div> <!-- slider content end-->
                            </div> <!-- col end-->
                        </div> <!-- row end-->
                    </div>
                    <!-- container end -->
                </div>
                <!-- hero table cell end -->
            </div>
            <!-- hero table end -->
        </div>

    </div>
    </div>
    <!-- Slider section end -->

    <!--
     Feature  Services
    -->
    <section class="spl_law_feature_layout_two">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-12 feature-list">
                    <div class=" feature-list">
                        <div class="box">
                            <div class="icon-box">
                                <i class="icofont-handcuff"></i>
                            </div>
                            <div class="content-bottom">
                                <h3><a href="#">Development &amp; CMS</a></h3>
                                <p>Direct mailing hypothese channels return on invest.</p>
                                <div class="btn_type_two">
                                    <a href="#">
                                        <span class="btn_text">Contact Us</span>
                                        <span class="btn_icon"><i class="icofont-rounded-right"></i></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 feature-list">
                    <div class=" feature-list">
                        <div class="box">
                            <div class="icon-box">
                                <i class="icofont-law-scales"></i>
                            </div>
                            <div class="content-bottom">
                                <h3><a href="#">Development &amp; CMS</a></h3>
                                <p>Direct mailing hypothese channels return on invest.</p>
                                <div class="btn_type_two">
                                    <a href="#">
                                        <span class="btn_text">Contact Us</span>
                                        <span class="btn_icon"><i class="icofont-rounded-right"></i></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 feature-list">
                    <div class=" feature-list">
                        <div class="box">
                            <div class="icon-box">
                                <i class="icofont-court"></i>
                            </div>
                            <div class="content-bottom">
                                <h3><a href="#">Development &amp; CMS</a></h3>
                                <p>Direct mailing hypothese channels return on invest.</p>
                                <div class="btn_type_two">
                                    <a href="#">
                                        <span class="btn_text">Contact Us</span>
                                        <span class="btn_icon"><i class="icofont-rounded-right"></i></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section class="spl_law_about_layout_two" style="background-image: url(assets/images/about_bg.jpg)">
        <div class="thin_layer" style="background: #0a0f1d; opacity: 0.9"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-md-6 col-sm-12">

                    <div class="img-background-wrapper">
                        <div class="img_background" style="background-image: url(assets/images/about_bg.jpg)">
                            <div class="border_img_overlay"></div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-7 col-md-6  col-sm-12">
                    <h2>LAW ADVICE FOR OUR CLIENT</h2>
                    <h3>We will always find a way out for the solution of the problem.</h3>
                    <p>
                        Nulla ut fringilla dui. Nullam sit amet pulvinar velit, at rutrum lacus. Aliquam uidem rerum
                        facilis est et expedita distinctio. Nam libero tempore, cum soluta Nulla ut fringilla dui. Nulla
                        ut fringilla dui. Nullam sit amet pulvinar velit, at rutrum lacus. Aliquam uidem rerum facilis
                        est et expedita distinctio. Nam libero tempore, cum soluta Nulla ut fringilla dui.
                    </p>
                    <div class="address-info">
                        <ul>
                            <li>Email Us: info@yourdoamin.com</li>
                            <li>Contact Us: +123 456 7890</li>
                        </ul>
                    </div>
                    <div class="btn_type_two">
                        <a href="#">
                            <span class="btn_text">Contact Us</span>
                            <span class="btn_icon"><i class="icofont-rounded-right"></i></span>
                        </a>
                    </div>
                    <div class="achivement-items">
                        <ul>
                            <li>
                                <div class="timer counter">12</div>
                                <span class="medium">Years of Experiance</span>
                            </li>
                            <li>
                                <div class="timer counter">230</div>
                                <span class="medium">Global customers</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="spl_law_practise_area layout_two">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="section-title-wrapper">
                        <h2 class="section-title-two"><span>We are expert at</span> Legal Practise Area</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="practise_area wow fadeInUp">
                        <div class="img-background-wrapper">
                            <div class="img_background" style="background-image: url(assets/images/blog-1.jpg)"></div>
                        </div>
                        <h3><a href="#">Gun Law</a></h3>
                        <p>In conjunction with his vast know-how, our company leverages the robust legal expertise of
                            working in different courts.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="practise_area wow fadeInUp">
                        <div class="img-background-wrapper">
                            <div class="img_background" style="background-image: url(assets/images/blog-2.jpg)"></div>
                        </div>
                        <h3><a href="#">Gun Law</a></h3>
                        <p>In conjunction with his vast know-how, our company leverages the robust legal expertise of
                            working in different courts.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="practise_area wow fadeInUp">
                        <div class="img-background-wrapper">
                            <div class="img_background" style="background-image: url(assets/images/blog-3.jpg)"></div>
                        </div>
                        <h3><a href="#">Gun Law</a></h3>
                        <p>In conjunction with his vast know-how, our company leverages the robust legal expertise of
                            working in different courts.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="practise_area wow fadeInUp">
                        <div class="img-background-wrapper">
                            <div class="img_background" style="background-image: url(assets/images/blog-4.jpg)"></div>
                        </div>
                        <h3><a href="#">Gun Law</a></h3>
                        <p>In conjunction with his vast know-how, our company leverages the robust legal expertise of
                            working in different courts.</p>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <section class="spl_law_contact_detail layout_two" style="background: url(assets/images/contactus_bg.png) no-repeat;background-size: cover">
        <div class="thin_layer" style="opacity: 0.8;background: #0a0f1d"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <h2>Are You Interest To Contact With Us?</h2>
                    <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of
                        classical Latin literature from 45 BC.</p>
                    <div class="add-contact">
                        <span class="call-us wow fadeInUp" data-wow-delay="0.1s"><i class="fas fa-map-marker-alt"></i><div
                                    class="contact_info_detail">
                                <h4>Our Location</h4>
                                <span><a href="#">New Road, Touterie Victoria 8520 Australia</a></span>
                            </div></span>
                        <span class="call-us wow fadeInUp" data-wow-delay="0.3s"><i class="fas fa-phone-volume"></i><div
                                    class="contact_info_detail">
                                <h4>Our Location</h4>
                                <span><a href="#">+987652541</a></span>
                            </div></span>
                        <span class="call-us wow fadeInUp" data-wow-delay="0.5s"><i class="fas fa-envelope"></i><div
                                    class="contact_info_detail">
                                <h4>Email</h4>
                                <span><a href="#">info@hehe.com</a></span>
                            </div></span>

                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 ">
                    <div class="contact_form">
                        <div role="form" class="wpcf7" id="wpcf7-f59-o1" lang="en-US" dir="ltr">
                            <div class="screen-reader-response"></div>
                            <h3 class="text-center">REQUEST A QUOTE</h3>
                            <form action="" method="post" class="wpcf7-form" novalidate="novalidate">
                                <div style="display: none;">
                                    <input type="hidden" name="_wpcf7" value="59">
                                    <input type="hidden" name="_wpcf7_version" value="5.1.1">
                                    <input type="hidden" name="_wpcf7_locale" value="en_US">
                                    <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f59-o1">
                                    <input type="hidden" name="_wpcf7_container_post" value="0">
                                    <input type="hidden" name="g-recaptcha-response" value="">
                                </div>
                                <p><label>
                                <span class="wpcf7-form-control-wrap your-name">
                                    <input type="text" class="wpcf7-form-control wpcf7-text" placeholder="Your Name">
                                </span>
                                    </label>
                                </p>
                                <p><label>
                                <span class="wpcf7-form-control-wrap your-email">
                                    <input type="email" name="your-email" class="wpcf7-form-control wpcf7-text"
                                           placeholder="Your Email (required)">
                                </span>
                                    </label>
                                </p>
                                <p><label>
                                <span class="wpcf7-form-control-wrap your-subject">
                                    <input type="text" name="your-subject" value=""
                                           class="wpcf7-form-control wpcf7-text" placeholder="Subject">
                                </span>
                                    </label>
                                </p>
                                <p><label>
                                <span class="wpcf7-form-control-wrap your-message">
                                    <textarea name="your-message" class="wpcf7-form-control wpcf7-textarea"
                                              placeholder="Message"></textarea>
                                </span>
                                    </label></p>
                                <p><input type="submit" value="Apponment" class="wpcf7-form-control wpcf7-submit"><span
                                            class="ajax-loader"></span></p>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="spl_law_case_study layout_two">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title-wrapper">
                        <h2 class="section-title-two"><span>We are expert at</span>Case Study</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="portfolio">

                        <ul class="text-center">
                            <li class="active">
                                <button data-filter="*">All</button>
                            </li>
                            <li>
                                <button data-filter=".wordpress">wordpress</button>
                            </li>
                            <li>
                                <button data-filter=".vb">vb</button>
                            </li>
                            <li>
                                <button data-filter=".joomla">joomla</button>
                            </li>
                        </ul>

                        <div class="portfolio-grid">
                            <div class="portfolio-wrap">
                                <div class="col-lg-4 col-md-4 col-sm-12 image-list wordpress">
                                    <div class="image-list"><img src="assets/images/case_1.jpg"
                                                                 alt="#"/>
                                        <div class="portfolio-overlay">
                                            <div class="portfolio-overlay-inner">
                                                <div class="portfolio-overlay-content">
                                                    <div class="portfolio-link">
                                                        <a title="Details" href="#"><i
                                                                    class="fa fa-link"></i></a>
                                                        <a href="#"><i
                                                                    class="fa fa-search-plus"></i></a>
                                                    </div><!--End portfolio-link -->
                                                    <div class="portfolio-caption">
                                                        <h3><a href="javascript:void(0)">Project Title5</a></h3>
                                                    </div><!--End portfolio-caption -->
                                                </div><!--End portfolio-overlay-content -->
                                            </div><!--End portfolio-overlay-inner -->
                                        </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 image-list vb joomla">
                                    <div class="image-list"><img src="assets/images/case_2.jpg"
                                                                 alt="#"/>
                                        <div class="portfolio-overlay">
                                            <div class="portfolio-overlay-inner">
                                                <div class="portfolio-overlay-content">
                                                    <div class="portfolio-link">
                                                        <a title="Details" href="#"><i
                                                                    class="fa fa-link"></i></a>
                                                        <a href="#"><i
                                                                    class="fa fa-search-plus"></i></a>
                                                    </div><!--End portfolio-link -->
                                                    <div class="portfolio-caption">
                                                        <h3><a href="javascript:void(0)">Project Title5</a></h3>
                                                    </div><!--End portfolio-caption -->
                                                </div><!--End portfolio-overlay-content -->
                                            </div><!--End portfolio-overlay-inner -->
                                        </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 image-list wordpress">
                                    <div class="image-list"><img src="assets/images/case_3.jpg"
                                                                 alt="#"/>
                                        <div class="portfolio-overlay">
                                            <div class="portfolio-overlay-inner">
                                                <div class="portfolio-overlay-content">
                                                    <div class="portfolio-link">
                                                        <a title="Details" href="#"><i
                                                                    class="fa fa-link"></i></a>
                                                        <a href="#"><i
                                                                    class="fa fa-search-plus"></i></a>
                                                    </div><!--End portfolio-link -->
                                                    <div class="portfolio-caption">
                                                        <h3><a href="javascript:void(0)">Project Title5</a></h3>
                                                    </div><!--End portfolio-caption -->
                                                </div><!--End portfolio-overlay-content -->
                                            </div><!--End portfolio-overlay-inner -->
                                        </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 image-list vb">
                                    <div class="image-list"><img src="assets/images/case_4.jpg"
                                                                 alt="#"/>
                                        <div class="portfolio-overlay">
                                            <div class="portfolio-overlay-inner">
                                                <div class="portfolio-overlay-content">
                                                    <div class="portfolio-link">
                                                        <a title="Details" href="#"><i
                                                                    class="fa fa-link"></i></a>
                                                        <a href="#"><i
                                                                    class="fa fa-search-plus"></i></a>
                                                    </div><!--End portfolio-link -->
                                                    <div class="portfolio-caption">
                                                        <h3><a href="javascript:void(0)">Project Title5</a></h3>
                                                    </div><!--End portfolio-caption -->
                                                </div><!--End portfolio-overlay-content -->
                                            </div><!--End portfolio-overlay-inner -->
                                        </div>
                                        </a>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-12 image-list wordpress joomla">
                                    <div class="image-list"><img src="assets/images/case_5.jpg"
                                                                 alt="#"/>
                                        <div class="portfolio-overlay">
                                            <div class="portfolio-overlay-inner">
                                                <div class="portfolio-overlay-content">
                                                    <div class="portfolio-link">
                                                        <a title="Details" href="#"><i
                                                                    class="fa fa-link"></i></a>
                                                        <a href="#"><i
                                                                    class="fa fa-search-plus"></i></a>
                                                    </div><!--End portfolio-link -->
                                                    <div class="portfolio-caption">
                                                        <h3><a href="javascript:void(0)">Project Title5</a></h3>
                                                    </div><!--End portfolio-caption -->
                                                </div><!--End portfolio-overlay-content -->
                                            </div><!--End portfolio-overlay-inner -->
                                        </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 image-list vb joomla">
                                    <div class="image-list"><img src="assets/images/case_6.jpg"
                                                                 alt="#"/>
                                        <div class="portfolio-overlay">
                                            <div class="portfolio-overlay-inner">
                                                <div class="portfolio-overlay-content">
                                                    <div class="portfolio-link">
                                                        <a title="Details" href="#"><i
                                                                    class="fa fa-link"></i></a>
                                                        <a href="#"><i
                                                                    class="fa fa-search-plus"></i></a>
                                                    </div><!--End portfolio-link -->
                                                    <div class="portfolio-caption">
                                                        <h3><a href="javascript:void(0)">Project Title5</a></h3>
                                                    </div><!--End portfolio-caption -->
                                                </div><!--End portfolio-overlay-content -->
                                            </div><!--End portfolio-overlay-inner -->
                                        </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </section>



    <div class="spl_law_video_bg">
        <video class="video-fluid" autoplay loop muted>
            <source src="assets/images/video.mp4" type="video/mp4"/>
        </video>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-md-12">
                    <div class="video_text">
                        <h2>Read our expertly written blog or follow us on social media</h2>
                        <p>Maybe it won’t get that far, but those who care about these international law disputes think
                            China and the U.S. are on a collision course because both sides hew closely to contradictory
                            readings of international law. One would assume the conflict won’t go nuclear.</p>
                        <div class="btn_type_two">
                            <a href="#">
                                <span class="btn_text">Contact Us</span>
                                <span class="btn_icon"><i class="icofont-rounded-right"></i></span>
                            </a>
                        </div>
                    </div>

                </div>
                <div class="col-lg-6 col-md-6 col-md-12">

                    <div class="blog_list_post">
                        <ul>
                            <li>
                                <a href="#">
                                    <div class="img-background-wrapper">
                                        <div class="img_background shine"
                                             style="background-image: url(assets/images/about_bg.jpg)">
                                        </div>
                                    </div>
                                    <div class="box-conetnt">
                                        <div class="post-info-date">
                                            <span class="post-info-date-day">18</span>
                                            <span class="post-info-date-month">Jun</span>
                                        </div>
                                        <div class="post-content">
                                            <h3> Training in The Law in order to become a Domestic or International
                                                Legal
                                                Adviser </h3>
                                            <div class="post_meta"><span><i class="far fa-folder-open"></i></span> in <span>Travel, law , contract</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="box-conetnt">
                                        <div class="post-info-date">
                                            <span class="post-info-date-day">18</span>
                                            <span class="post-info-date-month">Jun</span>
                                        </div>
                                        <div class="post-content">
                                            <h3> Training in The Law in order to become a Domestic or International
                                                Legal
                                                Adviser </h3>
                                            <div class="post_meta"><span><i class="far fa-folder-open"></i></span> in <span>Travel, law , contract</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="spl_law_who_are_we">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="progres_bar">
                        <div class="skill">
                            <p>Web design</p>
                            <div class="skill-bar wow slideInLeft   animated" style="width: 95%; visibility: visible;">
                                <span class="skill-count">95%</span>
                            </div>
                        </div>
                        <div class="skill">
                            <p>Software development</p>
                            <div class="skill-bar  wow slideInLeft   animated" style="width: 85%; visibility: visible;">
                                <span class="skill-count">85%</span>
                            </div>
                        </div>
                        <div class="skill">
                            <p>Content writing</p>
                            <div class="skill-bar  wow slideInLeft   animated" style="width: 90%; visibility: visible;">
                                <span class="skill-count">90%</span>
                            </div>
                        </div>
                        <div class="skill">
                            <p>Seo</p>
                            <div class="skill-bar  wow slideInLeft   animated" style="width: 97%; visibility: visible;">
                                <span class="skill-count">97%</span>
                            </div>
                        </div>
                        <div class="skill">
                            <p>Theme Development</p>
                            <div class="skill-bar  wow slideInLeft   animated" style="width: 90%; visibility: visible;">
                                <span class="skill-count">90%</span>
                            </div>
                        </div>
                        <div class="skill">
                            <p>Plugin Development</p>
                            <div class="skill-bar  wow slideInLeft   animated" style="width: 86%; visibility: visible; ">
                                <span class="skill-count">86%</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <h4>Let’s <span>WORK TOGETHER</span> on your project</h4>
                    <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id
                        est laborum. Sed ut perspiciatis undeomnis iste natus error sit voluptatem accusantium doloremque
                        laudantium <br><br>
                        Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim
                        id est laborum. Sed ut perspiciatis undeomnis iste natus error sit voluptatem accusantium
                        doloremque laudantium
                    </p>
                    <div class="btn_type_two">
                        <a href="#">
                            <span class="btn_text">Contact Us</span>
                            <span class="btn_icon"><i class="icofont-rounded-right"></i></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="spl_law_cs_team">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="section-title-wrapper">
                        <h2 class="section-title-two"><span>We are expert at</span> Our Team</h2>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="owl-carousel owl-theme team-carousel">

                        <div class="item">
                            <a href="#">
                                <div class="team_inner">
                                    <div class="team-image">
                                        <figure>
                                            <img src="assets/images/team_law_1.png" alt=""/>
                                        </figure>
                                    </div>
                                    <div class="team-info">
                                    <h4>Frank Norway</h4>
                                    <span>Chairman</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="item">
                            <a href="#">
                                <div class="team_inner">
                                    <div class="team-image">
                                        <figure>
                                            <img src="assets/images/team_law_2.png" alt=""/>
                                        </figure>
                                    </div>
                                    <div class="team-info">
                                    <h4>Frank Norway</h4>
                                    <span>Chairman</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="item">
                            <a href="#">
                                <div class="team_inner">
                                    <div class="team-image">
                                        <figure>
                                            <img src="assets/images/team_law_3.png" alt=""/>
                                        </figure>
                                    </div>
                                    <div class="team-info">
                                    <h4>Frank Norway</h4>
                                    <span>Chairman</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="item">
                            <a href="#">
                                <div class="team_inner">
                                    <div class="team-image">
                                        <figure>
                                            <img src="assets/images/team_law_4.png" alt=""/>
                                        </figure>
                                    </div>
                                    <div class="team-info">
                                    <h4>Frank Norway</h4>
                                    <span>Chairman</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="item">
                            <a href="#">
                                <div class="team_inner">
                                    <div class="team-image">
                                        <figure>
                                            <img src="assets/images/team_law_5.png" alt=""/>
                                        </figure>
                                    </div>
                                    <div class="team-info">
                                    <h4>Frank Norway</h4>
                                    <span>Chairman</span>
                                    </div>
                                </div>
                            </a>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="spl_law_video layout_two">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="section-title-wrapper">
                        <h2 class="section-title-two"><span>Any Questions</span> Frequently Asked Question</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="video wow fadeInUp" style="visibility: visible; ">
                        <div class="img-background-wrapper">
                            <div class="img_background" style="background-image: url(assets/images/video-1.png)">
                                <div class="border_img_overlay"></div>
                            </div>
                        </div>
                        <div class="thin_layer" style="background: #000;opacity: 0.6"></div>
                        <div class="button-video">
                            <a href="http://www.youtube.com/watch?v=0O2aH4XLbto" target="_blank" class="popup-youtube  box-shadow-ripples"><i class="fas fa-play "></i></a>
                        </div>
                    </div>

                </div>
                <div class="col-lg-6 col-md-6 col-sm-12">


                    <div class="accordion-container layout_two">
                        <div class="set wow fadeInUp" style="visibility: visible; ">
                            <a href="javascript:void(0)" class="active">
                                Vestibulum
                            </a>
                            <div class="content" style="display: block">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                    incididunt ut labore et dolore magna.Lorem ipsum dolor sit amet, consectetur
                                    adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                            </div>
                        </div>
                        <div class="set wow fadeInUp" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; ">
                            <a href="javascript:void(0)">
                                Phasellus
                            </a>
                            <div class="content">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                    incididunt ut labore et dolore magna.Lorem ipsum dolor sit amet, consectetur
                                    adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                            </div>
                        </div>
                        <div class="set wow fadeInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; ">
                            <a href="javascript:void(0)">
                                Praesent
                            </a>
                            <div class="content">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                    incididunt ut labore et dolore magna.Lorem ipsum dolor sit amet, consectetur
                                    adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                            </div>
                        </div>
                        <div class="set wow fadeInUp" data-wow-delay="0.7s" style="visibility: visible; animation-delay: 0.7s; ">
                            <a href="javascript:void(0)">
                                Curabitur
                            </a>
                            <div class="content">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                    incididunt ut labore et dolore magna.Lorem ipsum dolor sit amet, consectetur
                                    adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php include 'footer.php' ?>