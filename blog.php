<?php include 'header-pro-1.php';
include 'breadcumb.php'?>
    <div class="container">
        <div class="row">
            <div id="primary" class="content-area col-lg-8 col-md-8 col-sm-12 ">
                <main id="main" class="site-main">
                    <article class=" blog_list_post">
                        <div class="img-background-wrapper">
                            <div class="img_background shine"
                                 style="background-image: url(assets/images/about_bg.jpg)">
                            </div>
                        </div>
                        <div class="box-conetnt">
                            <div class="post-info-date">
                                <span class="post-info-date-day">18</span>
                                <span class="post-info-date-month">Jun</span>
                            </div>
                            <div class="video-content">
                                <h3><a href="#">Training in The Law in order to become a Domestic or International
                                        Legal
                                        Adviser </a> </h3>
                                <div class="post_meta"><span><i class="far fa-folder-open"></i></span> in <span>Travel, law , contract</span>
                                </div>
                                <p>Maybe it won’t get that far, but those who care about these international law
                                    disputes think China and the U.S. are on a collision course because both sides hew
                                    closely to contradictory readings of international law. One would assume the
                                    conflict won’t go nuclear.</p>
                            </div>
                        </div>
                    </article>
                    <article class=" blog_list_post">
                        <div class="img-background-wrapper">
                            <div class="img_background shine"
                                 style="background-image: url(assets/images/about_bg.jpg)">
                            </div>
                        </div>
                        <div class="box-conetnt">
                            <div class="post-info-date">
                                <span class="post-info-date-day">18</span>
                                <span class="post-info-date-month">Jun</span>
                            </div>
                            <div class="post-content">
                                <h3><a href="#">Training in The Law in order to become a Domestic or International
                                        Legal
                                        Adviser </a> </h3>
                                <div class="post_meta"><span><i class="far fa-folder-open"></i></span> in <span>Travel, law , contract</span>
                                </div>
                                <p>Maybe it won’t get that far, but those who care about these international law
                                    disputes think China and the U.S. are on a collision course because both sides hew
                                    closely to contradictory readings of international law. One would assume the
                                    conflict won’t go nuclear.</p>
                            </div>
                        </div>
                    </article>
                    <article class=" blog_list_post">
                        <div class="img-background-wrapper">
                            <div class="img_background shine"
                                 style="background-image: url(assets/images/about_bg.jpg)">
                            </div>
                        </div>
                        <div class="box-conetnt">
                            <div class="post-info-date">
                                <span class="post-info-date-day">18</span>
                                <span class="post-info-date-month">Jun</span>
                            </div>
                            <div class="post-content">
                                <h3><a href="#">Training in The Law in order to become a Domestic or International
                                        Legal
                                        Adviser </a> </h3>
                                <div class="post_meta"><span><i class="far fa-folder-open"></i></span> in <span>Travel, law , contract</span>
                                </div>
                                <p>Maybe it won’t get that far, but those who care about these international law
                                    disputes think China and the U.S. are on a collision course because both sides hew
                                    closely to contradictory readings of international law. One would assume the
                                    conflict won’t go nuclear.</p>
                            </div>
                        </div>
                    </article>
                    <article class=" blog_list_post">
                        <div class="img-background-wrapper">
                            <div class="img_background shine"
                                 style="background-image: url(assets/images/about_bg.jpg)">
                            </div>
                        </div>
                        <div class="box-conetnt">
                            <div class="post-info-date">
                                <span class="post-info-date-day">18</span>
                                <span class="post-info-date-month">Jun</span>
                            </div>
                            <div class="post-content">
                                <h3><a href="#">Training in The Law in order to become a Domestic or International
                                        Legal
                                        Adviser </a> </h3>
                                <div class="post_meta"><span><i class="far fa-folder-open"></i></span> in <span>Travel, law , contract</span>
                                </div>
                                <p>Maybe it won’t get that far, but those who care about these international law
                                    disputes think China and the U.S. are on a collision course because both sides hew
                                    closely to contradictory readings of international law. One would assume the
                                    conflict won’t go nuclear.</p>
                            </div>
                        </div>
                    </article>


                </main><!-- #main -->
            </div><!-- #primary -->
            <?php include 'sidebar.php'; ?>
        </div>
    </div>
<?php include 'footer.php'; ?>