<section class="breadcrumb" style="background-image: url(assets/images/breadcrumbs.jpg);background-repeat: no-repeat; background-size: cover; background-attachment: fixed; background-position: center;">
    <div class="thin_layer" style="background: #000;opacity: 0.6"></div>
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-sm-12 col-xs-12 breadcrumb_wrapper">
                <h2 class="entry-title">Blog</h2>
                <nav id="breadcrumb">
                    <div role="navigation" aria-label="Breadcrumbs" class="breadcrumb-trail breadcrumbs"
                         itemprop="breadcrumb">
                        <ul class="bb-breadcrumb-list" >
                            <meta name="numberOfItems" content="2">
                            <meta name="itemListOrder" content="Ascending">
                            <li class="trail-item trail-begin">
                                <a  href="#" ><span itemprop="name">Home</span></a>
                                <meta itemprop="position" content="1">
                            </li>
                            <li><span itemprop="item"><span
                                            itemprop="name">Blog</span></span>
                                <meta itemprop="position" content="2">
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</section>