<!doctype html>
<html>
<head>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <link href="assets/library/font-awesome/css/fontawsome.css" type="text/css" rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,500i,600,600i,700,700i,800"
          rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i,900,900i" rel="stylesheet">

    <link href="assets/library/bootstrap/css/bootstrap.min.css" type="text/css" rel="stylesheet"/>
    <link href="assets/library/owlcarousel/css/owl.carousel.min.css" rel="stylesheet" type="text/css">
    <link href="assets/library/icofont/icofont.min.css" rel="stylesheet" type="text/css">
    <!-------------- animate css ------------------------------>
    <link href="assets/css/animate.css" rel="stylesheet" type="text/css">
    <!-------------- magnific popup ------------------------------>
    <link href="assets/library/magnific-popup/magnefic.min.css" rel="stylesheet" type="text/css">

    <link href="style.css" type="text/css" rel="stylesheet"/>
    <link href="assets/css/responisve-pro.css" type="text/css" rel="stylesheet"/>

</head>

<body class="layout_two">
<div id="page" class="site">

    <header class="header-center layout_two">
        <div class="spl_law_top_bar">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 top_bar_left">
                        <ul>
                            <li><i class="far fa-clock"></i> Mon - Tues : 6.00 am - 10.00 pm</li>
                            <li><i class="fas fa-map-marker-alt"></i> 150 Street, London, USA</li>
                        </ul>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 top_bar_right">


                        <!--<form role="search" method="get" class="search-form float-right" action="#">
                            <label>
                                <span class="screen-reader-text">Search for:</span>
                                <input type="search" class="search-field" placeholder="Search …" value="" name="s">
                            </label>
                            <input type="submit" class="search-submit" value="Search">
                        </form>-->
                        <div class="btn_type_two">
                            <a href="#">
                                <span class="btn_text">Contact Us</span>
                                <span class="btn_icon"><i class="icofont-rounded-right"></i></span>
                            </a>
                        </div>
                        <ul class=" social-media">
                            <li><a href="#"><i class="fab fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
        <div class="spl_law_main_header">
            <div class="container">
                <div class="row no-gutters">
                    <div class="column-left-icon">
                        <div class="site-branding">
                            <!--<a href="#" class="custom-logo-link"><img width="290" height="50" src="assets/images/logo_light.png" class="custom-logo" alt="Hitstoreqq" ></a>-->
                            <h1 class="site-title"><a href="index.php" rel="home">Lawyer <span>Frim</span></a></h1>
                            <p class="site-description">Just another WordPress site</p>
                        </div><!-- .site-branding -->
                    </div>
                    <div class="column-right-navigation">
                        <nav id="site-navigation" class="main-navigation">
                            <button class="main-menu-toggle" aria-controls="primary-menu" aria-expanded="false"><i
                                    class="fa fa-bars"></i></button>
                            <div class="main-menu-container-collapse">
                                <ul id="primary-menu" class="menu nav-menu" aria-expanded="false">
                                    <li class="menu-item-has-children"><a href="#">Home</a>
                                        <ul class="sub-menu">
                                            <li><a href="index.php">Default</a></li>
                                            <li><a href="index-pro-1.php">Home 1</a></li>
                                            <li><a href="index-pro-2.php">Home 2</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="blog.php">Blog</a></li>
                                    <li><a href="blog.php">Single</a></li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                    <div class="column-button-search"><a href="#"><i class="icons fa fa-search"></i></a></div>
                    <div class="search-pop-up widget_search ">
                        <form role="search" method="get" class="search-form" action="#">
                            <label>
                                <span class="screen-reader-text">Search for:</span>
                                <input type="search" class="search-field" placeholder="Search …" value="" name="s">
                            </label>
                            <input type="submit" class="search-submit" value="Search">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div id="content" class="site-content">