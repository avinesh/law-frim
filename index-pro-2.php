<?php include 'header-pro-2.php' ?>
    <!--
     Banner slider starts here
    -->

    <div id="spl_law_bannerSlider" class="spl_law_bannerSlider owl-carousel features-slider">
        <!-- slider item start -->
        <div class="banner-item" style="background-image: url(assets/images/slider-4.png);">
            <div class="bannerOuter">
                <div class="bannerInner">
                    <div class="thin_layer" style="background: #000;opacity: 0.6"></div>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="slider-content center">
                                    <h2 class="banner-title">
                                        Insurance Law Expertise…It’s Our Policy
                                    </h2>
                                    <p>
                                        Benefit of the socie where we operate. A success website obusly needs great
                                        design to be one top10 IT companies in The world
                                    </p>
                                    <div class="btn_type_two">
                                        <a href="#">
                                            <span class="btn_text">Contact Us</span>
                                            <span class="btn_icon"><i class="icofont-rounded-right"></i></span>
                                        </a>
                                    </div>
                                </div> <!-- slider content end-->
                            </div> <!-- col end-->
                        </div> <!-- row end-->
                    </div>
                    <!-- container end -->
                </div>
                <!-- hero table cell end -->
            </div>
            <!-- hero table end -->
        </div>
        <div class="banner-item" style="background-image: url(assets/images/slider-1.png);">
            <div class="bannerOuter">
                <div class="bannerInner">
                    <div class="thin_layer" style="background: #000;opacity: 0.6"></div>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="slider-content center">
                                    <h2 class="banner-title">
                                        Committed to Helping Our Clients Succeed
                                    </h2>
                                    <p>
                                        Benefit of the socie where we operate. A success website obusly needs great
                                        design to be one top10 IT companies in The world
                                    </p>
                                    <div class="btn_type_two">
                                        <a href="#">
                                            <span class="btn_text">Contact Us</span>
                                            <span class="btn_icon"><i class="icofont-rounded-right"></i></span>
                                        </a>
                                    </div>
                                </div> <!-- slider content end-->
                            </div> <!-- col end-->
                        </div> <!-- row end-->
                    </div>
                    <!-- container end -->
                </div>
                <!-- hero table cell end -->
            </div>
            <!-- hero table end -->
        </div>
        <div class="banner-item" style="background-image: url(assets/images/slider-3.png);">
            <div class="bannerOuter">
                <div class="bannerInner">
                    <div class="thin_layer" style="background: #000;opacity: 0.6"></div>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="slider-content center">
                                    <h2 class="banner-title">
                                        Insurance Law Expertise…It’s Our Policy
                                    </h2>
                                    <p>
                                        Benefit of the socie where we operate. A success website obusly needs great
                                        design to be one top10 IT companies in The world
                                    </p>
                                    <div class="btn_type_two">
                                        <a href="#">
                                            <span class="btn_text">Contact Us</span>
                                            <span class="btn_icon"><i class="icofont-rounded-right"></i></span>
                                        </a>
                                    </div>
                                </div> <!-- slider content end-->
                            </div> <!-- col end-->
                        </div> <!-- row end-->
                    </div>
                    <!-- container end -->
                </div>
                <!-- hero table cell end -->
            </div>
            <!-- hero table end -->
        </div>
    </div>
    <!-- Slider section end -->

    <!--
     Feature  Services
    -->
    <section class="spl_law_feature_layout_two layout_three">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-12 feature-list">
                    <div class="box">
                        <div class="icon-box">
                            <i class="icofont-handcuff"></i>
                        </div>
                        <div class="content-bottom">
                            <h3><a href="#">Development &amp; CMS</a></h3>
                            <p>Direct mailing hypothese channels return on invest.Direct mailing hypothese channels
                                return on invest.</p>

                        </div>
                    </div>

                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 feature-list">
                    <div class="box">
                        <div class="icon-box">
                            <i class="icofont-law-scales"></i>
                        </div>
                        <div class="content-bottom">
                            <h3><a href="#">Development &amp; CMS</a></h3>
                            <p>Direct mailing hypothese channels return on invest.Direct mailing hypothese channels
                                return on invest.</p>

                        </div>
                    </div>

                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 feature-list">
                    <div class="box">
                        <div class="icon-box">
                            <i class="icofont-court"></i>
                        </div>
                        <div class="content-bottom">
                            <h3><a href="#">Development &amp; CMS</a></h3>
                            <p>Direct mailing hypothese channels return on invest.Direct mailing hypothese channels
                                return on invest.</p>

                        </div>

                    </div>
                </div>

            </div>
        </div>
    </section>

    <section class="spl_law_about_layout_two layout_three" style="background-image: url(assets/images/about_bg.jpg)">
        <div class="thin_layer" style="background: #0a0f1d; opacity: 0.9"></div>
        <div class="container">
            <div class="row">


                <div class="col-lg-6 col-md-6  col-sm-12">
                    <h2>We will always find a way out for the solution of the problem.</h2>
                    <p>
                        Nulla ut fringilla dui. Nullam sit amet pulvinar velit, at rutrum lacus. Aliquam uidem rerum
                        facilis est et expedita distinctio. Nam libero tempore, cum soluta Nulla ut fringilla dui. Nulla
                        ut fringilla dui. Nullam sit amet pulvinar velit, at rutrum lacus. Aliquam uidem rerum facilis
                        est et expedita distinctio. Nam libero tempore, cum soluta Nulla ut fringilla dui.
                    </p>

                    <div class="btn_type_two">
                        <a href="#">
                            <span class="btn_text">Contact Us</span>
                            <span class="btn_icon"><i class="icofont-rounded-right"></i></span>
                        </a>
                    </div>

                </div>
                <div class="col-lg-6 col-md-6 col-sm-12">

                    <div class="img-background-wrapper">
                        <div class="img_background shine " style="background-image: url(assets/images/about_bg.jpg)">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="spl_law_practise_area layout_three">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title-wrapper">
                        <h2 class="section-title-two"><span>We are expert at</span> Legal Practise Area</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="practise_area wow fadeInUp" style="visibility: visible;">
                        <div class="icon-area">
                            <i class="icofont-pistol"></i>
                        </div>
                        <h3><a href="#">Gun Law</a></h3>
                        <p>Sustainable development is the pathway to the future</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 ">
                    <div class="practise_area wow fadeInUp" data-wow-delay="0.3s"
                         style="visibility: visible; animation-delay: 0.3s; ">
                        <div class="icon-area">
                            <i class="fas fa-child"></i>
                        </div>
                        <h3><a href="#">Child Support</a></h3>
                        <p>Sustainable development is the pathway to the future</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 ">
                    <div class="practise_area  wow fadeInUp" data-wow-delay="0.6s"
                         style="visibility: visible; animation-delay: 0.6s;">
                        <div class="icon-area">
                            <i class="fas fa-venus-mars"></i></div>
                        <h3><a href="#">Divorce</a></h3>
                        <p>Sustainable development is the pathway to the future</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 ">
                    <div class="practise_area  wow fadeInUp" data-wow-delay="0s"
                         style="visibility: visible; animation-delay: 0s; ">
                        <div class="icon-area">
                            <i class="fas fa-user-secret"></i>
                        </div>
                        <h3><a href="#">Cyber Crime Law</a></h3>
                        <p>Sustainable development is the pathway to the future</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 ">
                    <div class="practise_area  wow fadeInUp" data-wow-delay="0.3s"
                         style="visibility: visible; animation-delay: 0.3s;">
                        <div class="icon-area">
                            <i class="icofont-handcuff"></i>
                        </div>
                        <h3><a href="#">Serious Crime</a></h3>
                        <p>Sustainable development is the pathway to the future</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 ">
                    <div class="practise_area wow fadeInUp" data-wow-delay="0.6s"
                         style="visibility: visible; animation-delay: 0.6s; ">
                        <div class="icon-area">
                            <i class="icofont-thief"></i>
                        </div>
                        <h3><a href="#">Robbery</a></h3>
                        <p>Sustainable development is the pathway to the future</p>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="spl_law_video layout_two layout_three">

        <div class="row no-gutters">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="video wow fadeInLeft" style="visibility: visible; ">
                    <div class="img-background-wrapper">
                        <div class="img_background shine" style="background-image: url(assets/images/video-1.png)">
                            <div class="border_img_overlay"></div>
                        </div>
                    </div>
                    <div class="thin_layer" style="background: #000;opacity: 0.6"></div>
                    <div class="button-video">
                        <a href="http://www.youtube.com/watch?v=0O2aH4XLbto" target="_blank"
                           class="popup-youtube  box-shadow-ripples"><i class="fas fa-play "></i></a>
                    </div>
                </div>

            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="content-descritpion">
                    <h2>Let’s work together on your project</h2>
                    <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id
                        est laborum. Sed ut perspiciatis undeomnis iste natus error sit voluptatem accusantium
                        doloremque
                        laudantium <br><br>
                        Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim
                        id est laborum. Sed ut perspiciatis undeomnis iste natus error sit voluptatem accusantium
                        doloremque laudantium
                    </p>
                    <div class="btn_type_two">
                        <a href="#">
                            <span class="btn_text">Contact Us</span>
                            <span class="btn_icon"><i class="icofont-rounded-right"></i></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>

    <section class="spl_law_case_study layout_two">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title-wrapper">
                        <h2 class="section-title-two"><span>Look At Our Work</span> Case Study</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="portfolio">

                        <ul class="text-center">
                            <li class="active">
                                <button data-filter="*">All</button>
                            </li>
                            <li>
                                <button data-filter=".wordpress">wordpress</button>
                            </li>
                            <li>
                                <button data-filter=".vb">vb</button>
                            </li>
                            <li>
                                <button data-filter=".joomla">joomla</button>
                            </li>
                        </ul>

                        <div class="portfolio-grid">
                            <div class="portfolio-wrap">
                                <div class="col-lg-4 col-md-4 col-sm-12 image-list wordpress">
                                    <div class="image-list"><img src="assets/images/case_1.jpg"
                                                                 alt="#"/>
                                        <div class="portfolio-overlay">
                                            <div class="portfolio-overlay-inner">
                                                <div class="portfolio-overlay-content">
                                                    <div class="portfolio-link">
                                                        <a title="Details" href="#"><i
                                                                    class="fa fa-link"></i></a>
                                                        <a href="#"><i
                                                                    class="fa fa-search-plus"></i></a>
                                                    </div><!--End portfolio-link -->
                                                    <div class="portfolio-caption">
                                                        <h3><a href="javascript:void(0)">Project Title5</a></h3>
                                                    </div><!--End portfolio-caption -->
                                                </div><!--End portfolio-overlay-content -->
                                            </div><!--End portfolio-overlay-inner -->
                                        </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 image-list vb joomla">
                                    <div class="image-list"><img src="assets/images/case_2.jpg"
                                                                 alt="#"/>
                                        <div class="portfolio-overlay">
                                            <div class="portfolio-overlay-inner">
                                                <div class="portfolio-overlay-content">
                                                    <div class="portfolio-link">
                                                        <a title="Details" href="#"><i
                                                                    class="fa fa-link"></i></a>
                                                        <a href="#"><i
                                                                    class="fa fa-search-plus"></i></a>
                                                    </div><!--End portfolio-link -->
                                                    <div class="portfolio-caption">
                                                        <h3><a href="javascript:void(0)">Project Title5</a></h3>
                                                    </div><!--End portfolio-caption -->
                                                </div><!--End portfolio-overlay-content -->
                                            </div><!--End portfolio-overlay-inner -->
                                        </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 image-list wordpress">
                                    <div class="image-list"><img src="assets/images/case_3.jpg"
                                                                 alt="#"/>
                                        <div class="portfolio-overlay">
                                            <div class="portfolio-overlay-inner">
                                                <div class="portfolio-overlay-content">
                                                    <div class="portfolio-link">
                                                        <a title="Details" href="#"><i
                                                                    class="fa fa-link"></i></a>
                                                        <a href="#"><i
                                                                    class="fa fa-search-plus"></i></a>
                                                    </div><!--End portfolio-link -->
                                                    <div class="portfolio-caption">
                                                        <h3><a href="javascript:void(0)">Project Title5</a></h3>
                                                    </div><!--End portfolio-caption -->
                                                </div><!--End portfolio-overlay-content -->
                                            </div><!--End portfolio-overlay-inner -->
                                        </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 image-list vb">
                                    <div class="image-list"><img src="assets/images/case_4.jpg"
                                                                 alt="#"/>
                                        <div class="portfolio-overlay">
                                            <div class="portfolio-overlay-inner">
                                                <div class="portfolio-overlay-content">
                                                    <div class="portfolio-link">
                                                        <a title="Details" href="#"><i
                                                                    class="fa fa-link"></i></a>
                                                        <a href="#"><i
                                                                    class="fa fa-search-plus"></i></a>
                                                    </div><!--End portfolio-link -->
                                                    <div class="portfolio-caption">
                                                        <h3><a href="javascript:void(0)">Project Title5</a></h3>
                                                    </div><!--End portfolio-caption -->
                                                </div><!--End portfolio-overlay-content -->
                                            </div><!--End portfolio-overlay-inner -->
                                        </div>
                                        </a>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-12 image-list wordpress joomla">
                                    <div class="image-list"><img src="assets/images/case_5.jpg"
                                                                 alt="#"/>
                                        <div class="portfolio-overlay">
                                            <div class="portfolio-overlay-inner">
                                                <div class="portfolio-overlay-content">
                                                    <div class="portfolio-link">
                                                        <a title="Details" href="#"><i
                                                                    class="fa fa-link"></i></a>
                                                        <a href="#"><i
                                                                    class="fa fa-search-plus"></i></a>
                                                    </div><!--End portfolio-link -->
                                                    <div class="portfolio-caption">
                                                        <h3><a href="javascript:void(0)">Project Title5</a></h3>
                                                    </div><!--End portfolio-caption -->
                                                </div><!--End portfolio-overlay-content -->
                                            </div><!--End portfolio-overlay-inner -->
                                        </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 image-list vb joomla">
                                    <div class="image-list"><img src="assets/images/case_6.jpg"
                                                                 alt="#"/>
                                        <div class="portfolio-overlay">
                                            <div class="portfolio-overlay-inner">
                                                <div class="portfolio-overlay-content">
                                                    <div class="portfolio-link">
                                                        <a title="Details" href="#"><i
                                                                    class="fa fa-link"></i></a>
                                                        <a href="#"><i
                                                                    class="fa fa-search-plus"></i></a>
                                                    </div><!--End portfolio-link -->
                                                    <div class="portfolio-caption">
                                                        <h3><a href="javascript:void(0)">Project Title5</a></h3>
                                                    </div><!--End portfolio-caption -->
                                                </div><!--End portfolio-overlay-content -->
                                            </div><!--End portfolio-overlay-inner -->
                                        </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </section>


    <div class="spl_law_video_bg">
        <video class="video-fluid" autoplay loop muted>
            <source src="assets/images/video.mp4" type="video/mp4"/>
        </video>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-md-12">
                    <div class="video_text">
                        <h2>Read our expertly written blog or follow us on social media</h2>
                        <p>Maybe it won’t get that far, but those who care about these international law disputes think
                            China and the U.S. are on a collision course because both sides hew closely to contradictory
                            readings of international law. One would assume the conflict won’t go nuclear.</p>
                        <div class="btn_type_two">
                            <a href="#">
                                <span class="btn_text">Contact Us</span>
                                <span class="btn_icon"><i class="icofont-rounded-right"></i></span>
                            </a>
                        </div>
                    </div>

                </div>
                <div class="col-lg-6 col-md-6 col-md-12">

                    <div class="blog_list_post">
                        <ul>
                            <li>
                                <a href="#">
                                    <div class="img-background-wrapper">
                                        <div class="img_background shine"
                                             style="background-image: url(assets/images/about_bg.jpg)">
                                        </div>
                                    </div>
                                    <div class="box-conetnt">
                                        <div class="post-info-date">
                                            <span class="post-info-date-day">18</span>
                                            <span class="post-info-date-month">Jun</span>
                                        </div>
                                        <div class="video-content">
                                            <h3> Training in The Law in order to become a Domestic or International
                                                Legal
                                                Adviser </h3>
                                            <div class="post_meta"><span><i class="far fa-folder-open"></i></span> in <span>Travel, law , contract</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="box-conetnt">
                                        <div class="post-info-date">
                                            <span class="post-info-date-day">18</span>
                                            <span class="post-info-date-month">Jun</span>
                                        </div>
                                        <div class="video-content">
                                            <h3> Training in The Law in order to become a Domestic or International
                                                Legal
                                                Adviser </h3>
                                            <div class="post_meta"><span><i class="far fa-folder-open"></i></span> in <span>Travel, law , contract</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="spl_law_who_are_we">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="section-title-wrapper">
                        <h2 class="section-title-two"><span>Any Questions</span> Frequently Asked Question</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="progres_bar">
                        <div class="skill">
                            <p>Web design</p>
                            <div class="skill-bar wow slideInLeft   animated"
                                 style="width: 95%; visibility: visible;">
                                <span class="skill-count">95%</span>
                            </div>
                        </div>
                        <div class="skill">
                            <p>Software development</p>
                            <div class="skill-bar  wow slideInLeft   animated"
                                 style="width: 85%; visibility: visible; ">
                                <span class="skill-count">85%</span>
                            </div>
                        </div>
                        <div class="skill">
                            <p>Content writing</p>
                            <div class="skill-bar  wow slideInLeft   animated"
                                 style="width: 90%; visibility: visible; ">
                                <span class="skill-count">90%</span>
                            </div>
                        </div>
                        <div class="skill">
                            <p>Seo</p>
                            <div class="skill-bar  wow slideInLeft   animated"
                                 style="width: 97%; visibility: visible;">
                                <span class="skill-count">97%</span>
                            </div>
                        </div>
                        <div class="skill">
                            <p>Theme Development</p>
                            <div class="skill-bar  wow slideInLeft   animated"
                                 style="width: 90%; visibility: visible;">
                                <span class="skill-count">90%</span>
                            </div>
                        </div>
                        <div class="skill">
                            <p>Plugin Development</p>
                            <div class="skill-bar  wow slideInLeft   animated"
                                 style="width: 86%; visibility: visible;">
                                <span class="skill-count">86%</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12">


                    <div class="accordion-container layout_two">
                        <div class="set wow fadeInUp" style="visibility: visible; ">
                            <a href="javascript:void(0)" class="active">
                                Vestibulum
                            </a>
                            <div class="content" style="display: block">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                    incididunt ut labore et dolore magna.Lorem ipsum dolor sit amet, consectetur
                                    adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                            </div>
                        </div>
                        <div class="set wow fadeInUp" data-wow-delay="0.3s"
                             style="visibility: visible; animation-delay: 0.3s; ">
                            <a href="javascript:void(0)">
                                Phasellus
                            </a>
                            <div class="content">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                    incididunt ut labore et dolore magna.Lorem ipsum dolor sit amet, consectetur
                                    adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                            </div>
                        </div>
                        <div class="set wow fadeInUp" data-wow-delay="0.5s"
                             style="visibility: visible; animation-delay: 0.5s;">
                            <a href="javascript:void(0)">
                                Praesent
                            </a>
                            <div class="content">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                    incididunt ut labore et dolore magna.Lorem ipsum dolor sit amet, consectetur
                                    adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                            </div>
                        </div>
                        <div class="set wow fadeInUp" data-wow-delay="0.7s"
                             style="visibility: visible; animation-delay: 0.7s;">
                            <a href="javascript:void(0)">
                                Curabitur
                            </a>
                            <div class="content">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                    incididunt ut labore et dolore magna.Lorem ipsum dolor sit amet, consectetur
                                    adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="spl_law_cs_team layout_three">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="section-title-wrapper">
                        <h2 class="section-title-two"><span>We are expert at</span> Our Team</h2>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="owl-carousel owl-theme team-carousel-layout-three">

                        <div class="item">

                            <div class="team_inner">
                                <div class="team-image">
                                    <a href="#">
                                        <div class="img-background-wrapper">
                                            <div class="img_background shine"
                                                 style="background-image: url(assets/images/team-1.png)">
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="team-info">
                                    <h4><a href="#">Frank Norway</a></h4>
                                    <span>Chairman</span>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos sequi
                                        placeat distinctio
                                        dolor</p>
                                    <ul class="team_social_icon ">
                                        <li><a href="#"><i class="fab fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
                                        <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                        <div class="item">
                            <div class="team_inner">
                                <div class="team-image">
                                    <a href="#">
                                        <div class="img-background-wrapper">
                                            <div class="img_background shine"
                                                 style="background-image: url(assets/images/team-2.png)">
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="team-info">
                                    <h4><a href="#">Frank Norway</a></h4>
                                    <span>Chairman</span>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos sequi
                                        placeat distinctio
                                        dolor</p>
                                    <ul class="team_social_icon ">
                                        <li><a href="#"><i class="fab fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
                                        <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="team_inner">
                                <div class="team-image">
                                    <a href="#">
                                        <div class="img-background-wrapper">
                                            <div class="img_background shine"
                                                 style="background-image: url(assets/images/team-3.png)">
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="team-info">
                                    <h4><a href="#">Frank Norway</a></h4>
                                    <span>Chairman</span>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos sequi
                                        placeat distinctio
                                        dolor</p>
                                    <ul class="team_social_icon ">
                                        <li><a href="#"><i class="fab fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
                                        <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="team_inner">
                                <div class="team-image">
                                    <a href="#">
                                        <div class="img-background-wrapper">
                                            <div class="img_background shine"
                                                 style="background-image: url(assets/images/team-4.png)">
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="team-info">
                                    <h4><a href="#">Frank Norway</a></h4>
                                    <span>Chairman</span>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos sequi
                                        placeat distinctio
                                        dolor</p>
                                    <ul class="team_social_icon ">
                                        <li><a href="#"><i class="fab fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
                                        <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="team_inner">
                                <div class="team-image">
                                    <a href="#">
                                        <div class="img-background-wrapper">
                                            <div class="img_background shine"
                                                 style="background-image: url(assets/images/team-5.png)">
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="team-info">
                                    <h4><a href="#">Frank Norway</a></h4>
                                    <span>Chairman</span>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos sequi
                                        placeat distinctio
                                        dolor</p>
                                    <ul class="team_social_icon ">
                                        <li><a href="#"><i class="fab fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
                                        <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="spl_law_testimonial_layout_two" style="background: url('assets/images/testimonial-bg.jpg');">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">

                    <div class="owl-carousel owl-theme testimonial_slider">
                        <div class="item">
                            <div class="client-img">
                                <img src="assets/images/testimonial-1.jpg" alt="" class="img-fluid ">
                            </div>

                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium illum
                                suscipit, quam quas fuga illo cupiditate labore eveniet, officia in modi eum
                                consequuntur quis alias delectus hic? Delectus, molestias eaque.</p>

                            <div class="client-text">
                                <h3>John Martin</h3>
                                <h4>Senior Developer</h4>
                            </div>
                        </div>
                        <div class="item">
                            <div class="client-img">
                                <img src="assets/images/testimonial-4.jpg" alt="" class="img-fluid ">
                            </div>

                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium illum
                                suscipit, quam quas fuga illo cupiditate labore eveniet, officia in modi eum
                                consequuntur quis alias delectus hic? Delectus, molestias eaque.</p>

                            <div class="client-text">
                                <h3>John Martin</h3>
                                <h4>Senior Developer</h4>
                            </div>
                        </div>
                        <div class="item">
                            <div class="client-img">
                                <img src="assets/images/testimonial-1.jpg" alt="" class="img-fluid ">
                            </div>

                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium illum
                                suscipit, quam quas fuga illo cupiditate labore eveniet, officia in modi eum
                                consequuntur quis alias delectus hic? Delectus, molestias eaque.</p>

                            <div class="client-text">
                                <h3>John Martin</h3>
                                <h4>Senior Developer</h4>
                            </div>
                        </div>
                        <div class="item">
                            <div class="client-img">
                                <img src="assets/images/testimonial-4.jpg" alt="" class="img-fluid ">
                            </div>

                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium illum
                                suscipit, quam quas fuga illo cupiditate labore eveniet, officia in modi eum
                                consequuntur quis alias delectus hic? Delectus, molestias eaque.</p>

                            <div class="client-text">
                                <h3>John Martin</h3>
                                <h4>Senior Developer</h4>
                            </div>
                        </div>


                    </div>
                </div>


            </div>
        </div>
    </section>

    <section class="spl_law_call_to_action">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-md-7 col-sm-12">
                    <h3>Read our expertly written blog or follow us on social media</h3>
                    <p>We will only send you important updates and notices.</p>
                </div>
                <div class="col-lg-3 col-md-5 col-sm-12">
                    <div class="btn_type_two">
                        <a href="#">
                            <span class="btn_text">Contact Us</span>
                            <span class="btn_icon"><i class="icofont-rounded-right"></i></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>


<?php include 'footer.php' ?>